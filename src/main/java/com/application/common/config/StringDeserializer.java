package com.application.common.config;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

public class StringDeserializer extends JsonDeserializer<String> {
    public StringDeserializer() {
    }

    public String deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        return StringUtils.trim(jp.getValueAsString());
    }
}
