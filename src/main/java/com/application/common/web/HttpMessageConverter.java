package com.application.common.web;

import com.application.common.model.BaseIgnoreRootJsonModel;
import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.PrettyPrinter;
import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.exc.InvalidDefinitionException;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.json.AbstractJackson2HttpMessageConverter;
import org.springframework.lang.Nullable;
import org.springframework.util.TypeUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import springfox.documentation.spring.web.json.Json;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

@Configuration
public class HttpMessageConverter extends AbstractJackson2HttpMessageConverter {
    private final ObjectMapper objectMapper;
    private final ObjectMapper objectMapperNoRoot;
    private final List<String> METRICS = Collections.singletonList("actuator");
    @Nullable
    private final PrettyPrinter ssePrettyPrinter;

    public HttpMessageConverter(@Qualifier("json_main") ObjectMapper objectMapper,@Qualifier("json_without_root") ObjectMapper objectMapperNoRoot) {
        super(objectMapper, new MediaType[]{MediaType.APPLICATION_JSON, new MediaType("application", "*+json")});
        this.objectMapper = objectMapper;
        this.objectMapperNoRoot = objectMapperNoRoot;
        DefaultPrettyPrinter prettyPrinter = new DefaultPrettyPrinter();
        prettyPrinter.indentObjectsWith(new DefaultIndenter("  ", "\ndata:"));
        this.ssePrettyPrinter = prettyPrinter;
    }
    protected Object readInternal(Class<?> clazz, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
        JavaType javaType = this.getJavaType(clazz, (Class)null);
        return this.readJavaType(javaType, inputMessage);
    }

    public Object read(Type type, @Nullable Class<?> contextClass, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
        JavaType javaType = this.getJavaType(type, contextClass);
        return this.readJavaType(javaType, inputMessage);
    }

    private Object readJavaType(JavaType javaType, HttpInputMessage inputMessage) throws IOException {
        Class<?> clazz  = javaType.getRawClass();
        boolean rootAware = true;
        if (BaseIgnoreRootJsonModel.class.isAssignableFrom(clazz)) {
            rootAware = false;
        }
        String data = IOUtils.toString(inputMessage.getBody(), StandardCharsets.UTF_8);
        ObjectMapper mapper = rootAware ? objectMapper : objectMapperNoRoot;
        return mapper.reader().forType(javaType).readValue(data);
    }

    protected void writeInternal(Object object, @Nullable Type type, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
        MediaType contentType = outputMessage.getHeaders().getContentType();
        ObjectMapper objectMapper = this.getObjectMapper(object);
        JsonEncoding encoding = this.getJsonEncoding(contentType);
        JsonGenerator generator = objectMapper.getFactory().createGenerator(outputMessage.getBody(), encoding);
        try {
            JavaType javaType = null;
            if (type != null && TypeUtils.isAssignable(type, object.getClass())) {
                javaType = this.getJavaType(type, (Class)null);
            }

            ObjectWriter objectWriter = objectMapper.writer();
            if (javaType != null && javaType.isContainerType()) {
                objectWriter = objectWriter.forType(javaType);
            }

            SerializationConfig config = objectWriter.getConfig();
            if (contentType != null && contentType.isCompatibleWith(MediaType.TEXT_EVENT_STREAM) && config.isEnabled(SerializationFeature.INDENT_OUTPUT)) {
                objectWriter = objectWriter.with(this.ssePrettyPrinter);
            }

            objectWriter.writeValue(generator, object);
            generator.flush();
        } catch (InvalidDefinitionException var12) {
            throw new HttpMessageConversionException("Type definition error: " + var12.getType(), var12);
        } catch (JsonProcessingException var13) {
            throw new HttpMessageNotWritableException("Could not write JSON: " + var13.getOriginalMessage(), var13);
        }
    }

    private ObjectMapper getObjectMapper(Object object) {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        boolean isMetric = false;
        Iterator var6 = this.METRICS.iterator();

        while(var6.hasNext()) {
            String metric = (String)var6.next();
            if (request.getRequestURI().startsWith("/" + metric)) {
                isMetric = true;
                break;
            }
        }

        if (isMetric) {
            return this.objectMapper;
        } else {
            return !(object instanceof BaseIgnoreRootJsonModel) && !(object instanceof Json) ? objectMapper : objectMapperNoRoot;
        }
    }
}
